#include <SoftwareSerial.h>

SoftwareSerial softSerial(9, 10); // RX, TX

int f1Pin = 5;
int f2Pin = 6;
int f3Pin = 7;

String inputString = "";
boolean stringComplete = false;
String commandString = "";

void setup() {

  softSerial.begin(9600);

  softSerial.println("FanController ready");

  analogWrite(f1Pin, 255);
  analogWrite(f2Pin, 255);
  analogWrite(f3Pin, 255);

}

void loop() {

  while (softSerial.available()) {
    char inChar = (char) softSerial.read();
    inputString += inChar;
    if (inChar == '\n') {
      stringComplete = true;
    }
    softSerial.println("Serial char received");
  }

  if (stringComplete) {

    softSerial.println("stringComplete!");

    stringComplete = false;
    getCommand();

    if (commandString.equals("1")) {

      softSerial.println("FAN1 erkannt");

      String value = getValue();
      int v = value.toInt();
      analogWrite(f1Pin, v);
      softSerial.println("FAN1 is now ");
      softSerial.println(v);
      
    } else if (commandString.equals("2")) {

      String value = getValue();
      int v = value.toInt();
      analogWrite(f2Pin, v);
      softSerial.println("FAN2 is now ");
      softSerial.println(v);
      
    } else if (commandString.equals("3")) {

      String value = getValue();
      int v = value.toInt();
      analogWrite(f3Pin, v);
      softSerial.println("FAN3 is now ");
      softSerial.println(v);
      
    }

    inputString = "";
    
  }

}

void getCommand() {
  if(inputString.length()>0) {
     commandString = inputString.substring(1,2);
  }
}

String getValue() {
  String value = inputString.substring(2,inputString.length()-1);
  return value;
}
