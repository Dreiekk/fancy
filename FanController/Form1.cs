﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using OpenHardwareMonitor.Hardware;
using System.Threading;
using System.Diagnostics;

namespace FanController {

	public partial class Form1 : Form {

		bool readyToDie = false;
		bool connected = false;
		SerialPort sp;
		int fanSpeed1 = 0;
		int fanSpeed2 = 0;
		bool loadingSettings = false;
		int temp = 0;

		Thread tempUpdateThread;

		public Form1() {

			InitializeComponent();
			loadSettings();

			tempUpdateThread = new Thread(tempUpdate);
			tempUpdateThread.Start();

		}

		public void tempUpdate() {

			while (true) {

				Computer c = new Computer() {
					CPUEnabled = true
				};
				c.Open();

				foreach (var hardware in c.Hardware) {

					if (hardware.HardwareType == HardwareType.CPU) {

						hardware.Update();

						foreach (var sensor in hardware.Sensors) {

							if (sensor.SensorType == SensorType.Temperature && sensor.Name.Contains("Package")) {

								label9.Text = hardware.Name + "\n" + sensor.Name;
								temp = (int)sensor.Value.GetValueOrDefault();

								break;

							}

						}

						break;

					}
				}

				Thread.Sleep(infoUpdateTimer.Interval);

			}

		}

		private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e) {
			showForm();
		}

		private void notifyIcon1_MouseClick(object sender, MouseEventArgs e) {
			//showForm();
		}

		private void showForm() {

			Show();
			
		}

		private void Form1_Load(object sender, EventArgs e) {

			

		}

		private void loadSettings() {

			loadingSettings = true;

			String autoConPortName = Properties.Settings.Default.autoStartPort;
			if (autoConPortName != "") {
				checkBox1.Checked = true;
				checkBox1.Text = $"automatisch beim Start verbinden ( { autoConPortName } )";

				comboBox1.Text = autoConPortName;
				serialConnect();

			}

			updateListBox();

			loadingSettings = false;

		}

		private void updateListBox() {
			String loadedTable = Properties.Settings.Default.speedTable;
			if (loadedTable != "") {

				String[] points = loadedTable.Split('#');
				foreach (String point in points) {
					String[] values = point.Split('-');

					try {
						listBox2.Items.Add($"T:{ values[0] } / F1:{ values[1] } / F2:{ values[2] }");
					} catch (Exception ex) {

					}

				}

			}
		}

		private void saveSettings() {
			Properties.Settings.Default.Save();
		}

		private void einstellungenToolStripMenuItem_Click(object sender, EventArgs e) {
			Show();
		}

		private void beendenToolStripMenuItem_Click(object sender, EventArgs e) {
			readyToDie = true;
			notifyIcon1.Visible = false;
			Application.Exit();
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e) {

			if (!readyToDie) {
				e.Cancel = true;
				Hide();
			}

			tempUpdateThread.Abort();

		}

		private void comboBox1_DropDown(object sender, EventArgs e) {

			comboBox1.Items.Clear();
			String[] portNames = SerialPort.GetPortNames();

			foreach (String portName in portNames) {
				comboBox1.Items.Add(portName);
			}

		}

		private void button2_Click(object sender, EventArgs e) {

			if (!connected) {
				serialConnect();
			} else {
				serialDisconnect();
			}

		}

		private void serialConnect() {

			try {
				sp = new SerialPort(comboBox1.Text);
				sp.BaudRate = 9600;
				sp.Open();
			} catch (Exception ex) {
				MessageBox.Show(null, "Fehler beim Verbinden mit SerialPort.", "FanController");
				return;
			}

			comboBox1.Enabled = false;
			button2.Text = "Trennen";
			connected = true;

			notifyIcon1.ShowBalloonTip(5, "FanController", "Mit " + sp.PortName + " verbunden.", ToolTipIcon.None);
			label1.Text = "Mit " + sp.PortName + " verbunden.";

		}

		private void serialDisconnect() {

			String portName = "FanController";

			try {
				portName = sp.PortName;
				sp.Close();
			} catch (Exception ex) {
				
			}
			
			sp = null;

			comboBox1.Enabled = true;
			button2.Text = "Verbinden";
			connected = false;

			notifyIcon1.ShowBalloonTip(5, "FanController", "Verbindung mit " + portName + " getrennt.", ToolTipIcon.None);
			label1.Text = "Zurzeit nicht verbunden.";

		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e) {

			if (loadingSettings) {
				return;
			}

			if (!checkBox1.Checked) {

				Properties.Settings.Default.autoStartPort = "";
				saveSettings();
				checkBox1.Text = "automatisch beim Start verbinden ( Port )";

			} else {

				String portName = comboBox1.Text;

				if (portName == "") {
					MessageBox.Show("Kein Port ausgewählt.");
					checkBox1.Checked = false;
				}

				Properties.Settings.Default.autoStartPort = portName;
				saveSettings();
				checkBox1.Text = $"automatisch beim Start verbinden ( { portName } )";

			}

		}

		private void infoUpdateTimer_Tick(object sender, EventArgs e) {

			if (checkBox2.Checked) {

				// manual

				fanSpeed1 = (int)numericUpDown2.Value;
				fanSpeed2 = (int)numericUpDown3.Value;

			} else {

				// calculated
				fanSpeed1 = 0;
				fanSpeed2 = 0;

			}

			label6.Text = "Temperatur: " + temp;
			label7.Text = "Fan1 Speed: " + fanSpeed1;
			label8.Text = "Fan2 Speed: " + fanSpeed2;

			if (connected) {
				try {
					sp.Write(fanSpeed1.ToString() + "-" + fanSpeed2.ToString());
				} catch (Exception ex) {
					serialDisconnect();
				}
			}

		}

		private void listBox2_SelectedIndexChanged(object sender, EventArgs e) {

			

		}

		private void button6_Click(object sender, EventArgs e) {

			

		}

		private void button5_Click(object sender, EventArgs e) {

			

		}
	}
}
